all: primitives encrypt

primitives: cipher hkdf hash pbkdf mac randomness

PRIMITIVEOBJS := obj/chacha.o obj/sha1.o obj/pkcs5_pbkdf2.o obj/blake2b.o obj/randomness.o primitives/hkdf/massar_rfc6234/hkdf.o primitives/hkdf/massar_rfc6234/sha384-512.o primitives/hkdf/massar_rfc6234/hmac.o primitives/hkdf/massar_rfc6234/sha1.o primitives/hkdf/massar_rfc6234/usha.o primitives/hkdf/massar_rfc6234/sha224-256.o

test: test/rng test/pbkdf test/mac test/cyph

test/rng: test/rng.c randomness
	gcc test/rng.c -I primitives/randomness obj/randomness.o -o test/rng

test/pbkdf: test/kd.c pbkdf
	gcc test/kd.c obj/pkcs5_pbkdf2.o obj/sha1.o -I primitives/pbkdf -I primitives/hash -o test/kd

test/mac: test/mac.c mac
	gcc test/mac.c obj/blake2b.o -I primitives/mac -o test/mac

test/cyph: test/cyph.c cipher
	gcc test/cyph.c obj/chacha.o -I primitives/cipher -o test/cyph

# CIPHER

cipher: obj/chacha.o

obj/chacha.o: primitives/cipher/chacha.c primitives/cipher/ecrypt-sync.h primitives/cipher/ecrypt-portable.h primitives/cipher/ecrypt-config.h primitives/cipher/ecrypt-machine.h
	gcc -c primitives/cipher/chacha.c -I primitives/cipher/ -o obj/chacha.o

# PASSWORD BASED KEY DERIVATION

pbkdf: obj/sha1.o obj/pkcs5_pbkdf2.o

obj/pkcs5_pbkdf2.o: primitives/pbkdf/pkcs5_pbkdf2.c primitives/hash/sha1.h
	gcc -c primitives/pbkdf/pkcs5_pbkdf2.c -I primitives/pbkdf/ -I primitives/hash/ -o obj/pkcs5_pbkdf2.o

# HMAC BASED KEY DERIVATION

hkdf:
	cd primitives/hkdf/massar_rfc6234 && make all

# HASH

hash: obj/sha1.o

obj/sha1.o: primitives/hash/sha1.c primitives/hash/sha1.h
	gcc -c primitives/hash/sha1.c -I primitives/hash -o obj/sha1.o

# MAC

mac: obj/blake2b.o

obj/blake2b.o: primitives/mac/blake2b.c primitives/mac/blake2b.h
	gcc -c primitives/mac/blake2b.c -I primitives/mac -o obj/blake2b.o

# RANDOMNESS

randomness: obj/randomness.o

obj/randomness.o: primitives/randomness/randomness.c primitives/randomness/randomness.h
	gcc -c primitives/randomness/randomness.c -I primitives/randomness -o obj/randomness.o

# ENCRYPT

utilities.o: utilities.c utilities.h
	gcc -c utilities.c -o utilities.o

encrypt: encrypt.c primitives utilities.o
	gcc -g encrypt.c $(PRIMITIVEOBJS) utilities.o -I primitives/cipher -I primitives/pbkdf -I primitives/hash -I primitives/mac -I primitives/randomness -I primitives/hkdf/massar_rfc6234 -o encrypt

#

#

clean:
	rm -f obj/chacha.o
	rm -f obj/sha1.o obj/pkcs5_pbkdf2.o
	rm -f obj/blake2b.o
	rm -f obj/randomness.o
	rm -f test/rng
	rm -f test/kd
	rm -f test/mac
	rm -f test/cyph
	cd primitives/hkdf/massar_rfc6234 && make clean
