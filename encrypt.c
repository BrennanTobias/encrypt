#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "randomness.h"
#include "pkcs5_pbkdf2.h"
#include "ecrypt-sync.h"
#include "blake2b.h"
#include "sha.h"

#include "utilities.h"

int hkdf(SHAversion whichSha,
	 const unsigned char *salt, int salt_len,
	 const unsigned char *ikm, int ikm_len,
	 const unsigned char *info, int info_len,
	 uint8_t okm[ ], int okm_len);

int encrypt_decrypt(int mode, FILE *in, FILE *out, char *password);

void usage() {
  puts("Usage:");
  puts(" ./encrypt (enc|dec) inputfilename outputfilename");
}

int main(int argc, char **argv) {
  FILE *in_fptr, *out_fptr;

  char *pass;
  size_t read, len;

  int mode, res;

  if(argc != 4) {
    usage();
    return EXIT_FAILURE;
  }

  if(!strcmp(argv[1], "enc")) {
    mode = 1;
  }
  else if(!strcmp(argv[1], "dec")) {
    mode = 0;
  }
  else {
    usage();
    return EXIT_FAILURE;
  }

  in_fptr = fopen(argv[2], "r");
  if(!in_fptr) {
    printf("Could not open input file: %s\n", argv[1]);
    return EXIT_FAILURE;
  }
  
  out_fptr = fopen(argv[3], "w");
  if(!out_fptr) {
    printf("Could not open output file: %s\n", argv[2]);
    return EXIT_FAILURE;
  }

  printf("Password: ");
  fflush(stdout);
  len = 0;
  if((read = getline(&pass, &len, stdin)) != -1) {
    if(pass[read-1] == '\n') {
      pass[read-1] = '\0';
    }
    else {
      puts("Problem reading password");
      return EXIT_FAILURE;
    }
  }
  
  res = encrypt_decrypt(mode, in_fptr, out_fptr, pass);

  fclose(out_fptr);

  return res;
}

int encrypt_decrypt(int mode, FILE *in, FILE *out, char *password) {
  int rounds;

  unsigned char nonce[32];
  unsigned char secret[ECRYPT_MAXKEYSIZE];
  unsigned char mackey[48];
  unsigned char mac[48];
  unsigned char mac_verify[48];
  
  ECRYPT_ctx ctx;
  unsigned char key[ECRYPT_MAXKEYSIZE] = { 0 };
  unsigned char iv[ECRYPT_MAXIVSIZE] = { 0 };
  
  size_t len;
  unsigned char *plain, *crypt;

  // read the entire file.
  // this is simpler, but for large files we should process a block at a time

  if(mode) {
    read_from_file(in, &plain, &len);
    crypt = malloc(len+(32+48));
  }
  else {
    read_from_file(in, &crypt, &len);

    memcpy(nonce, crypt, 32);
    
    memcpy(mac_verify, crypt+32, 48);
    
    plain = crypt+32+48;

    len -= 32+48;
    crypt = malloc(len);
  }
  
  /*
   * encrypt(password, string):
   *   nonce := generate_random_nonce()
   *   secret := pbkdf(nonce, password)
   *   mackey := kbkdf(secret, 'mackey')
   *   enckey := kbkdf(secret, 'enckey')
   *   iv := kbkdf(secret, 'iv')
   *   encrypted := cipher(iv, enckey, string)
   *   return (nonce || mac(mackey, encrypted) || encrypted)
   */

  if(mode) {
    generate_random_bytes(nonce, 32);
  }

  rounds = 10000;
  pkcs5_pbkdf2(password, strlen(password),
	       nonce, 32,
	       secret, ECRYPT_MAXKEYSIZE,
	       rounds);
  
  hkdf(SHA256, "mackey", 6, secret, ECRYPT_MAXKEYSIZE, NULL, 0, mackey, 48);
  hkdf(SHA256, "enckey", 6, secret, ECRYPT_MAXKEYSIZE, NULL, 0, key, ECRYPT_MAXIVSIZE);
  hkdf(SHA256, "inivec", 6, secret, ECRYPT_MAXKEYSIZE, NULL, 0, iv, ECRYPT_MAXIVSIZE);
  
  ECRYPT_init();
  ECRYPT_keysetup(&ctx,
		  key, ECRYPT_MAXKEYSIZE,
		  ECRYPT_MAXIVSIZE);
  ECRYPT_ivsetup(&ctx,
		 iv);
  ECRYPT_encrypt_bytes(&ctx,
		       plain,
		       crypt,
		       len);
  
  blake2b(mac, 48,
	  mackey, 48,
	  mode ? crypt : plain, len);
  if(!mode && memcmp(mac, mac_verify, 48)) {
    puts("mac did not verify!");
    return EXIT_FAILURE;
  }

  fwrite(nonce, 32, 1, out);
  fwrite(mac, 48, 1, out);
  fwrite(crypt, len, 1, out);
  
  return EXIT_SUCCESS;
}
