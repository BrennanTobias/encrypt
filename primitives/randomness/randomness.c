#include "randomness.h"

#include <stdio.h>

int generate_random_bytes(unsigned char *buf, size_t len) {
  FILE *urandom;
  size_t res;

  urandom = fopen("/dev/urandom", "r");
  if(!urandom) {
    return 1;
  }

  res = fread(buf, len, 1, urandom);
  fclose(urandom);

  if(res == len) {
    return 0;
  }
  else {
    return 1;
  }
}
