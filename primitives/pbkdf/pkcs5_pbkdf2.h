#include <stdint.h>
#include <sha1.h>

void
hmac_sha1(const u_int8_t *text, size_t text_len, const u_int8_t *key,
	  size_t key_len, u_int8_t digest[]);

int
pkcs5_pbkdf2(const char *pass, size_t pass_len, const uint8_t *salt,
	     size_t salt_len, uint8_t *key, size_t key_len, unsigned int rounds);
