#include <stdlib.h>
#include <stdio.h>

#include "randomness.h"

int main(void) {
  unsigned char buf[8];

  if(generate_random_bytes(buf, 8)) {
    printf("%02x%02x%02x%02x%02x%02x%02x%02x\n",
	   buf[0],buf[1],buf[2],buf[3],
	   buf[4],buf[5],buf[6],buf[7]);
    return EXIT_SUCCESS;
  }
  else {
    puts("failed to generate random bytes");
    return EXIT_FAILURE;
  }
}
