#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "ecrypt-sync.h"

int main(void) {
  int i;
  ECRYPT_ctx ctx;

  unsigned char key[ECRYPT_MAXKEYSIZE] = { 0 };
  unsigned char iv[ECRYPT_MAXIVSIZE] = { 0 };

  unsigned char plain[] = "hello world";
  unsigned char crypt[11];

  for(i = 0; i < ECRYPT_MAXKEYSIZE; i++) {
    key[i] = i;
  }
  for(i = 0; i < ECRYPT_MAXIVSIZE; i++) {
    iv[i] = i;
  }
  
  ECRYPT_init();
  
  ECRYPT_keysetup(&ctx,
		  key, ECRYPT_MAXKEYSIZE,
		  ECRYPT_MAXIVSIZE);
  
  ECRYPT_ivsetup(&ctx,
		 iv);
  
  ECRYPT_encrypt_bytes(&ctx,
		       plain,
		       crypt,
		       sizeof plain - 1);

  for(i = 0; i < sizeof plain - 1; i++) {
    printf("%02x", crypt[i]);
  }
  puts("");

  memset(plain, 0, sizeof plain - 1);
  
  ECRYPT_keysetup(&ctx,
		  key, ECRYPT_MAXKEYSIZE,
		  ECRYPT_MAXIVSIZE);
  
  ECRYPT_ivsetup(&ctx,
		 iv);
  
  ECRYPT_decrypt_bytes(&ctx,
		       crypt,
		       plain,
		       sizeof plain - 1);
  
  puts(plain);
  
  return EXIT_SUCCESS;
}
