#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "pkcs5_pbkdf2.h"

#define HMAC_SHA1_TEST(key, text, res)					\
  hmac_sha1(text, sizeof text - 1, key, sizeof key - 1, digest);	\
  if(memcmp(digest, res, 20)) {						\
    printf("HMAC-SHA1 failure at %d\n", __LINE__);			\
    return EXIT_FAILURE;						\
  }

#define PBKDF2_TEST(p, s, c, dkl, res)		\
  ret = pkcs5_pbkdf2(p, sizeof p - 1,		\
		     s, sizeof s - 1,		\
		     derivedkey, dkl,		\
		     c);			\
  if(ret) {							\
    printf("PBKDF2 failed to execute at %d\n", __LINE__);	\
    return EXIT_FAILURE;					\
  }								\
  if(memcmp(derivedkey, res, dkl)) {				\
    printf("PBKDF2 failure to execute at %d\n", __LINE__);	\
    return EXIT_FAILURE;					\
  }


int main(void) {
  unsigned char digest[SHA1_DIGEST_LENGTH];
  unsigned char derivedkey[4096];
  int ret;
  
  // HMAC-SHA1 test vectors from
  // https://tools.ietf.org/html/rfc2202.html

  HMAC_SHA1_TEST("\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b",
		 "Hi There",
		 "\xb6\x17\x31\x86\x55\x05\x72\x64\xe2\x8b\xc0\xb6\xfb\x37\x8c\x8e\xf1\x46\xbe\x00");
  HMAC_SHA1_TEST("Jefe",
		 "what do ya want for nothing?",
		 "\xef\xfc\xdf\x6a\xe5\xeb\x2f\xa2\xd2\x74\x16\xd5\xf1\x84\xdf\x9c\x25\x9a\x7c\x79");

  // PBKDF2 HMAC-SHA1 test vectors from https://www.ietf.org/rfc/rfc6070.txt

  PBKDF2_TEST("password",
	      "salt",
	      1,
	      20,
	      "\x0c\x60\xc8\x0f\x96\x1f\x0e\x71\xf3\xa9\xb5\x24\xaf\x60\x12\x06\x2f\xe0\x37\xa6");

  return EXIT_SUCCESS;
}
