#include <stdlib.h>
#include <stdio.h>

#include "blake2b.h"

int main(void) {
  if(blake2b_selftest()) {
    puts("blake2b selftest failed");
    return EXIT_FAILURE;
  }
  
  return EXIT_SUCCESS;
}
